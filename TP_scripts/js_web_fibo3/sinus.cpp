#include <cmath>
const double PI= 3.14;
double maFonction(double x, double a, double b){
	return sin(2*PI*(a*x+b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("maFonction", &maFonction);
}
